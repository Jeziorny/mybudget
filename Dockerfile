FROM python:3.9-alpine

WORKDIR /home

COPY src /home/src
COPY requirements.txt /home

RUN pip install -r requirements.txt
RUN ls

CMD ["python", "src/app.py"]
