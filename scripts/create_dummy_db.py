from werkzeug.security import generate_password_hash

from src.models import db, User, Budget, Access, Expense
from src.app import create_app

app = create_app()


def _main():
    _create_tables()
    _add_dummy_data()


def _create_tables():
    db.create_all(app=app)


def _add_dummy_data():
    with app.app_context():
        u1 = User(
            email="user1@gmail.com",
            password=generate_password_hash("password", method="sha256"),
        )
        u2 = User(
            email="user2@gmail.com",
            password=generate_password_hash("password", method="sha256"),
        )
        b1 = Budget(income=1000, category=0, user=u1, id=0)
        b2 = Budget(income=200, category=0, user=u2, id=1)
        b3 = Budget(income=420, category=1, user=u1, id=2)
        e1 = Expense(name="fuel", value=700, budget=b1)
        e2 = Expense(name="food", value=200, budget=b1)
        e3 = Expense(name="toys", value=100, budget=b2)
        e4 = Expense(name="weed", value=420, budget=b3)
        ub1 = Access(user_id=1, budget_id=2)
        db.session.add_all([u1, u2, b1, b2, b3, e1, e2, e3, e4, ub1])
        db.session.commit()


if __name__ == "__main__":
    _main()
