## My Budget
   Python 3.9.9 flask application for cope with user budgets. Endpoints:
##### POST:
- `/login` - create user session and return JWT token,
- `/signup` - creates user account,
- `/budget` - creates budget,
- `/share` - shares a budget,
##### GET:
- `/budgets` - returns budgets with pagination,
- `/others_budgets` - returns shared by others budgets with pagination.

### Things I'm proud of:
- development environment (CI),
- unit tests which specify app behaviour,
- layered app structure: views -> forms (logic) -> database.

### Things I would improve when have time:
- couple of validators for e.g. email, password complexity,
- moving common parts of fixtures to conftest.py,
- <del>`Expense` as separate database model<del>,
- filters


### Service execution manual (Linux)
1. In shell export following variables:
    ```shell
    export UID=$(id -u)
    export GID=$(id -g)
    ```
2. If you have example database (`mybudget/scripts/file.db`) put it in `/tmp` directory.
3. In shell run:
   ```shell
   docker-compose up --build --force-recreate
   ``` 
   from project root directory.   
   Server will be available on http://localhost/5000.  
4. In case you want to create dummy database install python requirements and run
   ```shell
   python3 scripts/create_dummy_db.py
   ```

### Database
During implementation I've make use of SQLite database placed in /tmp directory.
Server expects to see database in `/tmp/file.db`, which has the source in following line from `app.py`:
```python
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/file.db"
```

Because application is dockerized, I attached it to container using docker volumes:
```dockerfile
    ...
    volumes:
      - /tmp/file.db:/tmp/file.db # During development I put `file.db` to /tmp directory
    ...
```
You can check it out in `docker-compose.yml`.

I've got problems with writing permissions to volume file from docker container.
Even running
```shell
chmod a+rw file.db
```
for SQLite database file wasn't sufficient to properly perform write operations on database.
I've solved it by creating user in `docker-compose`, which has same uid and gid, as the user which created database.

