from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin

db = SQLAlchemy()


class Access(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    budget_id = db.Column(db.Integer, db.ForeignKey("budget.id"), nullable=False)

    user = db.relationship("User", viewonly=True, foreign_keys=[user_id])
    budget = db.relationship("Budget", viewonly=True, foreign_keys=[budget_id])
    __table_args__ = (db.UniqueConstraint("user_id", "budget_id"),)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    budgets = db.relationship("Budget", backref="user")
    access = db.relationship(
        "Budget", viewonly=True, secondary=Access.__table__, backref="User"
    )

    def __repr__(self):
        return f"<User(id={self.id}, email={self.email})>"


class Budget(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    income = db.Column(db.Integer, nullable=False)
    category = db.Column(db.Integer, nullable=False, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    access = db.relationship("User", secondary=Access.__table__, backref="Budget")
    expenses = db.relationship("Expense", backref="budget")

    def __repr__(self):
        return f"<Budget(id={self.id}, income={self.income}, category={self.category}, user_id={self.user_id})>"

    @property
    def json(self):
        return {
            "id": self.id,
            "income": self.income,
            "category": self.category,
            "expenses": [expense.json for expense in self.expenses],
        }


class Expense(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    value = db.Column(db.Integer)
    budget_id = db.Column(db.Integer, db.ForeignKey("budget.id"))

    @property
    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "value": self.value,
        }
