from flask_wtf import FlaskForm
from werkzeug.security import check_password_hash
from wtforms import StringField
from wtforms.validators import DataRequired

from src.models import User


class LoginForm(FlaskForm):
    class Meta:
        csrf = False  # cookie-less authorization

    email = StringField("email", validators=[DataRequired()])
    password = StringField("password", validators=[DataRequired()])

    @property
    def user(self):
        return User.query.filter(User.email == self.email.data).first()

    def validate(self, extra_validators=None):
        is_form_valid = super().validate(extra_validators)
        is_email_exist = self.user is not None
        if is_form_valid and is_email_exist:
            return check_password_hash(self.user.password, self.password.data)
        return False
