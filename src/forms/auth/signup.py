from flask_wtf import FlaskForm
from werkzeug.security import generate_password_hash
from wtforms import StringField
from wtforms.validators import DataRequired, ValidationError

from src.models import User, db


class SignupForm(FlaskForm):
    class Meta:
        csrf = False  # cookie-less authorization

    email = StringField("email", validators=[DataRequired()])
    password1 = StringField("password1", validators=[DataRequired()])
    password2 = StringField("password2", validators=[DataRequired()])

    def validate_email(self, field):
        user_with_same_email = User.query.filter(User.email == field.data).first()
        if user_with_same_email is not None:
            raise ValidationError("User with given email already exists")

    def validate(self, extra_validators=None):
        is_form_valid = super().validate(extra_validators)
        are_passwords_equal = self.password1.data == self.password2.data
        return is_form_valid and are_passwords_equal

    def save(self):
        new_user = User(
            email=self.email.data,
            password=generate_password_hash(self.password1.data, method="sha256"),
        )
        db.session.add(new_user)
        db.session.commit()
