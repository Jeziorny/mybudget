import json

from flask import Request
from flask_jwt_extended import current_user
from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, Form, FieldList, FormField, SelectField
from wtforms.validators import NumberRange

from src.models import Budget, db, Expense

_INVALID_INCOME_MESSAGE = "Income must greater or equal to 0."
_INVALID_EXPENSE_VALUE_MESSAGE = "Expense must be greater or equal to 0."


class ExpensesForm(Form):
    name = StringField("name")
    value = IntegerField(
        "value", validators=[NumberRange(min=0, message=_INVALID_EXPENSE_VALUE_MESSAGE)]
    )


class BudgetForm(FlaskForm):
    class Meta:
        csrf = False  # cookie-less authorization

    income = IntegerField(
        "income", validators=[NumberRange(min=0, message=_INVALID_INCOME_MESSAGE)]
    )
    expenses = FieldList(FormField(ExpensesForm), min_entries=1)
    category = SelectField("category", choices=[1, 2])
    user_id = IntegerField("user_id")

    def __init__(self, form, user_id, *args, **kwargs):
        self._data = self._parse_budget(form)
        self.user_id = user_id
        super().__init__(*args, **kwargs)

    @staticmethod
    def _parse_budget(req: Request) -> dict:
        return {
            "income": req.form.get("income"),
            "category": req.form.get("category"),
            "expenses": json.loads(req.form.get("expenses")),
            "user_id": current_user.id,
        }

    def save(self):
        budget = Budget(
            income=self.income.data,
            category=self.category.data,
            user_id=self.user_id.data,
            expenses=[
                Expense(name=expense["name"], value=expense["value"])
                for expense in self.expenses.data
            ],
        )
        db.session.add(budget)
        db.session.commit()

    def process(self, formdata=None, obj=None, data=None, extra_filters=None, **kwargs):
        super().process(
            formdata=formdata,
            obj=obj,
            data=self._data,
            extra_filters=extra_filters,
            **kwargs
        )
