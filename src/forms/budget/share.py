from flask_jwt_extended import current_user
from flask_wtf import FlaskForm
from sqlalchemy.exc import IntegrityError
from wtforms import IntegerField, StringField, Form
from wtforms.validators import DataRequired, Optional, ValidationError

from src.forms.budget.common import SaveStatus
from src.models import Access, User, db, Budget


class ShareBudgetForm(FlaskForm):
    class Meta:
        csrf = False  # cookie-less authorization

    email = StringField("email", validators=[DataRequired()])
    budget_id = IntegerField("budget_id", validators=[DataRequired()])

    def validate_budget_id(self, field):
        user_budget_ids = {
            budget.id
            for budget in Budget.query.filter(Budget.user_id == current_user.id).all()
        }
        if field.data not in user_budget_ids:
            raise ValidationError("Cannot share budget of other user")

    def validate_email(self, field):
        user_with_given_email = User.query.filter(User.email == field.data).first()
        if user_with_given_email.email == current_user.email:
            raise ValidationError("Cannot share your own budget to yourself.")

    def save(self) -> SaveStatus:
        uid = User.query.filter(User.email == self.email.data).first().id
        access = Access(budget_id=self.budget_id.data, user_id=uid)
        db.session.add(access)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return SaveStatus.ALREADY_EXISTS
        return SaveStatus.SUCCESS


class SharedBudgetsForm(Form):
    user_id = IntegerField("user_id")
    page = IntegerField("page", validators=[Optional()])

    @property
    def query(self):
        available = Access.query.filter(Access.user_id == self.user_id.data).all()
        budget_ids = [a.budget_id for a in available]
        budgets = Budget.query.filter(Budget.id.in_(budget_ids))
        pagination = budgets.paginate(page=self.page.data or 1)
        return pagination
