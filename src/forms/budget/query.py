from flask_jwt_extended import current_user
from flask_sqlalchemy import Pagination
from sqlalchemy import and_
from wtforms import IntegerField, Form, SelectField
from wtforms.validators import Optional

from src.models import Budget


class BudgetQueryForm(Form):
    page = IntegerField("page", validators=[Optional()])
    income_op = SelectField(
        "income_op", choices=["leq", "geq", "eq"], validators=[Optional()]
    )
    income_value = IntegerField("value", validators=[Optional()])

    @staticmethod
    def _add_user_filter(conditions: list):
        conditions.append(Budget.user_id == current_user.id)

    def _add_income_filter(self, conditions: list):
        if None in (self.income_op.data, self.income_value.data):
            return
        conditions.append(
            {
                "leq": Budget.income < self.income_value.data,
                "geq": Budget.income > self.income_value.data,
                "eq": Budget.income == self.income_value.data,
            }[self.income_op.data]
        )

    @property
    def query(self) -> Pagination:
        conditions = []
        self._add_user_filter(conditions)
        self._add_income_filter(conditions)
        query_result = Budget.query.filter(and_(*conditions))
        pagination = query_result.paginate(page=self.page.data or 1)
        return pagination
