from enum import Enum


class SaveStatus(Enum):
    SUCCESS = 0
    ALREADY_EXISTS = 1
