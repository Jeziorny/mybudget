from flask import Flask
from flask_jwt_extended import JWTManager

from src.models import User


def create_app() -> Flask:
    app = Flask(__name__)
    # URI chosen to be compatible with development and production environment
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/file.db"
    app.config[
        "SQLALCHEMY_TRACK_MODIFICATIONS"
    ] = False  # To avoid flask-related warning
    app.config["SECRET_KEY"] = "dummy-secret-key"

    from src.models import db

    db.init_app(app)

    from src.views.auth import auth as auth_blueprint

    app.register_blueprint(auth_blueprint)

    from src.views.budget import budget_logic as budget_blueprint

    app.register_blueprint(budget_blueprint)

    jwt = JWTManager(app)

    @jwt.user_identity_loader
    def user_identity_lookup(user):
        """
        :param user: User
        :return: user_id
        """
        return user.id

    @jwt.user_lookup_loader
    def user_lookup_callback(_jwt_header, jwt_data):
        """
        Returns User obj with given identity
        """
        identity = jwt_data["sub"]
        return User.query.filter_by(id=identity).one_or_none()

    return app


if __name__ == "__main__":
    add_app = create_app()
    add_app.run(host="0.0.0.0", debug=True)
