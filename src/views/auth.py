from http import HTTPStatus

from flask import Blueprint, jsonify
from flask_jwt_extended import create_access_token

from src.forms.auth.login import LoginForm
from src.forms.auth.signup import SignupForm

auth = Blueprint("auth", __name__)


@auth.route("/login", methods=["POST"])
def login():
    """
    Expects dictionary with `email` and `password` field.

    :return:
    - UNAUTHORIZED when:
      - password is invalid,
      - user with given email does not exist,
      - invalid form data
    - OK otherwise
    """
    form = LoginForm()
    if not form.validate():
        return jsonify(msg=form.errors), HTTPStatus.UNAUTHORIZED

    user = form.user
    access_token = create_access_token(identity=user)
    return jsonify(access_token=access_token), HTTPStatus.OK


@auth.route("/signup", methods=["POST"])
def signup():
    """
    Expects dictionary with `email`, `password1` and `password2` on input

    :return:
    - BAD_REQUEST:
      - password1 != password2,
      - user with given email already exists.
    - OK otherwise
    """
    form = SignupForm()
    if not form.validate():
        return jsonify(msg=form.errors), HTTPStatus.BAD_REQUEST

    form.save()
    return jsonify(msg=f"User added"), HTTPStatus.OK
