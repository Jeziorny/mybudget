from http import HTTPStatus

from flask import Blueprint, jsonify, request
from flask_jwt_extended import jwt_required, current_user

from src.forms.budget.common import SaveStatus
from src.forms.budget.post import BudgetForm
from src.forms.budget.query import BudgetQueryForm
from src.forms.budget.share import ShareBudgetForm, SharedBudgetsForm

budget_logic = Blueprint("budget", __name__)


@budget_logic.route("/budget", methods=["POST"])
@jwt_required()
def budget_post():
    """
    Expects to get budget in form. Here is example from tests:
    {
        "income": 2000,
        "category": 1,
        "expenses": [{"name": "food", "value": 1000}]
    }

    :return:
    - BAD_REQUEST when:
      - income < 0,
      - expense.value < 0,
      - category not in (0, 1),
      - there is no expenses,
    - CREATED otherwise
    """
    form = BudgetForm(request, user_id=current_user.id)
    form.process()
    if not form.validate():
        return jsonify(msg=form.errors), HTTPStatus.BAD_REQUEST

    form.save()
    return jsonify(msg="Budget added"), HTTPStatus.CREATED


@budget_logic.route("/budgets", methods=["GET"])
@jwt_required()
def budgets():
    """
    Expects to get budget query params:
        page: Optional[int],
        income_op: Union["leq", "geq", "eq"],
        income_value: Optional[int]

    :return:
    - BAD_REQUEST when:
      - Query params are invalid
    - OK otherwise
    """
    form = BudgetQueryForm(data=request.args)

    if not form.validate():
        return jsonify(msg=form.errors), HTTPStatus.BAD_REQUEST

    return jsonify([budget.json for budget in form.query.items]), HTTPStatus.OK


@budget_logic.route("/share", methods=["POST"])
@jwt_required()
def share_budget():
    """
    Expects to have following params:
        email: str
        budget_id: int

    :return:
    - BAD_REQUEST when:
        - user want to share budget to himself,
        - user want to share someone else budget,
        - user want to share already shared budget.
    - OK with max 20 budgets (page).
    """
    form = ShareBudgetForm()
    if not form.validate():
        return jsonify(msg=form.errors), HTTPStatus.BAD_REQUEST

    save_status = form.save()
    if save_status is SaveStatus.SUCCESS:
        return jsonify(msg="Budget shared"), HTTPStatus.CREATED
    return jsonify(msg="Budget already shared for given user"), HTTPStatus.BAD_REQUEST


@budget_logic.route("/others_budgets", methods=["GET"])
@jwt_required()
def others_budgets():
    """
    Optionally takes `page` in input params to denote page with shared budgets.

    :return:
    BAD_REQUEST on invalid form,
    OK with max 20 budgets (page).
    """
    form = SharedBudgetsForm(
        data={"user_id": current_user.id, "page": request.args.get("page")}
    )
    if not form.validate():
        return jsonify(msg=form.errors), HTTPStatus.BAD_REQUEST

    return jsonify([budget.json for budget in form.query.items]), HTTPStatus.OK
