import json
import os
from http import HTTPStatus

import pytest
from werkzeug.security import generate_password_hash

from src.app import create_app
from src.models import db, User, Budget, Expense


@pytest.fixture
def app():
    app = create_app()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
    db.create_all(app=app)
    return app


@pytest.fixture
def client(app):
    with app.app_context():
        db.session.add(
            User(
                email="email",
                password=generate_password_hash("password", method="sha256"),
            )
        )
        for i in range(40):
            db.session.add(
                Budget(
                    income=i,
                    expenses=[Expense(name="name", value=str(j)) for j in range(3)],
                    category=0,
                    user_id=1,
                )
            )
        db.session.commit()

    with app.test_client() as client:
        try:
            yield client
        finally:
            os.remove("/tmp/test.db")


def test_get_user_budgets_returns_twenty_budgets_on_page(client):
    expected_record_number_per_page = 20
    login_response = client.post(
        "/login", json={"email": "email", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    get_budgets_response = client.get(
        "/budgets", headers={"Authorization": f"Bearer {access_token}"}
    )

    assert login_response.status_code == HTTPStatus.OK
    assert get_budgets_response.status_code == HTTPStatus.OK
    assert len(json.loads(get_budgets_response.data)) == expected_record_number_per_page


def test_get_user_budgets_with_income_filter_with_pagination(client):
    expected_record_number_per_page = 20
    login_response = client.post(
        "/login", json={"email": "email", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    get_budgets_response = client.get(
        "/budgets",
        query_string={"income_op": "leq", "income_value": 30},
        headers={"Authorization": f"Bearer {access_token}"},
    )

    assert login_response.status_code == HTTPStatus.OK
    assert get_budgets_response.status_code == HTTPStatus.OK
    data = json.loads(get_budgets_response.data)
    assert len(data) == expected_record_number_per_page
    for budget in data:
        assert budget["income"] < 30
