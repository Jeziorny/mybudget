import json
import os
from http import HTTPStatus

import pytest
from werkzeug.security import generate_password_hash

from src.app import create_app
from src.models import db, User, Budget, Expense


@pytest.fixture
def client():
    app = create_app()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
    db.create_all(app=app)

    with app.app_context():
        u1 = User(
            email="email", password=generate_password_hash("password", method="sha256")
        )
        db.session.add(u1)
        db.session.commit()

    with app.test_client() as client:
        yield client

    os.remove("/tmp/test.db")


def assert_budgets_equal(budget, expected_budget):
    for attr in ("id", "income", "category", "expenses", "user_id"):
        assert (
            getattr(budget, attr) == getattr(expected_budget, attr),
            f"Expected budget {attr} to be {getattr(expected_budget, attr)}, got {getattr(budget, attr)}",
        )


def assert_expenses_equal(expense, expected_expense):
    for attr in ("id", "name", "value", "budget_id"):
        assert (
            getattr(expense, attr) == getattr(expected_expense, attr),
            f"Expected expense {attr} to be {getattr(expected_expense, attr)}, got {getattr(expense, attr)}",
        )


def test_add_budget_sunny_scenario(client):
    login_response = client.post(
        "/login", json={"email": "email", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    add_budget_response = client.post(
        "/budget",
        data={
            "income": 2000,
            "category": 1,
            "expenses": json.dumps(
                [
                    {
                        "name": "food",
                        "value": 1000,
                    },
                ]
            ),
        },
        headers={"Authorization": f"Bearer {access_token}"},
    )
    expected_budget = Budget(id=1, income=2000, category=1, user_id=1)
    expected_expense = Expense(name="food", value=1000, budget_id=1)

    budgets = Budget.query.all()
    expenses = Expense.query.all()
    assert login_response.status_code == HTTPStatus.OK
    assert add_budget_response.status_code == HTTPStatus.CREATED
    assert len(budgets) == 1
    assert len(expenses) == 1
    assert_budgets_equal(budgets[0], expected_budget)
    assert_expenses_equal(expenses[0], expected_expense)


def test_add_budget_returns_bad_request_for_invalid_form_data(client):
    invalid_income = -1000
    login_response = client.post(
        "/login", json={"email": "email", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    add_budget_response = client.post(
        "/budget",
        data={
            "income": invalid_income,
            "category": 1,
            "expenses": json.dumps(
                [
                    {
                        "name": "food",
                        "value": 1000,
                    }
                ]
            ),
        },
        headers={"Authorization": f"Bearer {access_token}"},
    )

    budgets = Budget.query.all()
    expenses = Expense.query.all()
    assert login_response.status_code == HTTPStatus.OK
    assert add_budget_response.status_code == HTTPStatus.BAD_REQUEST
    assert len(budgets) == 0
    assert len(expenses) == 0
