import json
import os
from http import HTTPStatus

import pytest
from werkzeug.security import generate_password_hash

from src.app import create_app
from src.models import db, User, Budget, Access, Expense


@pytest.fixture
def app():
    app = create_app()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
    db.create_all(app=app)
    return app


@pytest.fixture
def client(app):
    with app.app_context():
        db.session.add(
            User(
                email="user1@gmail.com",
                password=generate_password_hash("password", method="sha256"),
            )
        )
        db.session.add(
            User(
                email="user2@gmail.com",
                password=generate_password_hash("password", method="sha256"),
            )
        )
        db.session.add(
            Budget(
                income=1000,
                expenses=[Expense(name="food", value=500)],
                category=0,
                user_id=1,
                id=1,
            ),
        )
        db.session.add(
            Budget(
                income=2000,
                expenses=[Expense(name="gear", value=666)],
                category=0,
                user_id=2,
                id=2,
            ),
        )
        db.session.add(Access(user_id="1", budget_id="2"))
        db.session.commit()

    with app.test_client() as client:
        try:
            yield client
        finally:
            os.remove("/tmp/test.db")


def test_user_1_has_budget_2_in_shared_list(client):
    expected_budget = {
        "category": 0,
        "expenses": [
            {
                "id": 2,
                "name": "gear",
                "value": 666,
            }
        ],
        "id": 2,
        "income": 2000,
    }

    login_response = client.post(
        "/login", json={"email": "user1@gmail.com", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    others_budgets_response = client.get(
        "/others_budgets", headers={"Authorization": f"Bearer {access_token}"}
    )

    assert login_response.status_code == HTTPStatus.OK
    assert others_budgets_response.status_code == HTTPStatus.OK
    assert json.loads(others_budgets_response.data) == [expected_budget]


def test_user_1_shares_budget_1_to_user_2(client):
    login_response = client.post(
        "/login", json={"email": "user1@gmail.com", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    share_response = client.post(
        "/share",
        data={
            "budget_id": 1,
            "email": "user2@gmail.com",
        },
        headers={"Authorization": f"Bearer {access_token}"},
    )

    access_entry = Access.query.filter(Access.budget_id == 1).first()
    assert login_response.status_code == HTTPStatus.OK
    assert share_response.status_code == HTTPStatus.CREATED
    assert access_entry.user_id == 2


@pytest.mark.parametrize(
    "email, budget_id, expected_response_msg, expected_response_code",
    [
        (
            "user2@gmail.com",
            2,
            {"email": ["Cannot share your own budget to yourself."]},
            HTTPStatus.BAD_REQUEST,
        ),
        (
            "user1@gmail.com",
            1,
            {"budget_id": ["Cannot share budget of other user"]},
            HTTPStatus.BAD_REQUEST,
        ),
        (
            "user1@gmail.com",
            2,
            "Budget already shared for given user",
            HTTPStatus.BAD_REQUEST,
        ),
    ],
    ids=[
        "Sharing budget to yourself",
        "Sharing someones else budget",
        "Sharing already shared budget",
    ],
)
def test_sharing_budget_fails(
    email, budget_id, expected_response_msg, expected_response_code, client
):
    login_response = client.post(
        "/login", json={"email": "user2@gmail.com", "password": "password"}
    )
    access_token = json.loads(login_response.data).get("access_token")
    share_response = client.post(
        "/share",
        data={
            "budget_id": budget_id,
            "email": email,
        },
        headers={"Authorization": f"Bearer {access_token}"},
    )

    assert login_response.status_code == HTTPStatus.OK
    assert share_response.status_code == expected_response_code
    assert json.loads(share_response.data).get("msg") == expected_response_msg
