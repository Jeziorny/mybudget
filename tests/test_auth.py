import json
import os
from http import HTTPStatus

import pytest
from werkzeug.security import generate_password_hash

from src.app import create_app
from src.models import db, User

_DUMMY_EMAIL = "dummy_user@jezzior.com"
_DUMMY_PASSWORD = "szukampracy"


@pytest.fixture
def client():
    app = create_app()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
    db.create_all(app=app)

    with app.test_client() as client:
        yield client

    os.remove("/tmp/test.db")


@pytest.fixture
def client_with_user_in_database():
    app = create_app()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
    db.create_all(app=app)
    with app.app_context():
        u1 = User(
            email=_DUMMY_EMAIL,
            password=generate_password_hash(_DUMMY_PASSWORD, method="sha256"),
        )
        db.session.add(u1)
        db.session.commit()

    with app.test_client() as client:
        yield client

    os.remove("/tmp/test.db")


def test_signup_sunny_scenario(client):
    response = client.post(
        "/signup",
        data={
            "email": _DUMMY_EMAIL,
            "password1": _DUMMY_PASSWORD,
            "password2": _DUMMY_PASSWORD,
        },
    )

    assert response.status_code == HTTPStatus.OK
    u = User.query.filter(User.email == _DUMMY_EMAIL).first()
    assert u.password != _DUMMY_PASSWORD, "Password in database stored as plain text!"


def test_signup_when_user_with_given_mail_already_exists(client_with_user_in_database):
    response = client_with_user_in_database.post(
        "/signup",
        data={
            "email": _DUMMY_EMAIL,
            "password1": _DUMMY_PASSWORD,
            "password2": _DUMMY_PASSWORD,
        },
    )

    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.data) == {
        "msg": {"email": ["User with given email already exists"]}
    }


def test_signup_when_password_differs(client_with_user_in_database):
    response = client_with_user_in_database.post(
        "/signup",
        data={
            "email": _DUMMY_EMAIL,
            "password1": "najpierwmowiszjedno",
            "password2": "apotemdrugiezdecydujsie",
        },
    )

    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.data) == {
        "msg": {"email": ["User with given email already exists"]}
    }


def test_login_sunny_scenario(client_with_user_in_database):
    login_response = client_with_user_in_database.post(
        "/login", data={"email": _DUMMY_EMAIL, "password": _DUMMY_PASSWORD}
    )

    assert login_response.status_code == HTTPStatus.OK
    assert json.loads(login_response.data).get("access_token") is not None


@pytest.mark.parametrize(
    "email, password",
    [
        ("new_mail", _DUMMY_PASSWORD),
        (_DUMMY_EMAIL, "new_password"),
    ],
    ids=["Mail does not match", "Password does not match"],
)
def test_login_returns_unauthorized_for_invalid_credentials(
    email, password, client_with_user_in_database
):
    response = client_with_user_in_database.post(
        "/login", data={"email": email, "password": password}
    )

    assert response.status_code == HTTPStatus.UNAUTHORIZED
